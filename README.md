# Blink Arduino



## Compte-rendu

Compte rendus du cours architecture système embarqué, prise en main d'arduino ( programmation barre métal ) et programmation linux embarqué ( Raspberry PI3 B).

## Arduino
### Matériel et logiciels

Arduino Uno Rev3 SMD <br/>
Machine Virtuelle, Linux Mint <br/>
VSCodium, PlatformIO et GIT <br/>
Buildroot


## Prise en main de VSCodium et Platform IO

Une machine virtuelle Linux Mint nous a était fournie. Sur cette machine virtuelle est installée l'éditeur VSCodium. Cet eéditeur est une version allégée de l'éditeur VisualStudioCode. Sur l'éditeur est installé PlatforIO, est une extension qui permet de créer notamment des projets Arduino. L'extension GIT est aussi présente, elle permet de versionner notre projet et ainsi d'avoir une meilleure gestion sur son développement et sa maintenance. <br/>

Travaillant déja avec l'éditeur VSCode, la prise en main de sa version allégée présente sur la VM ne fut pas compliquée. Cependant, l'extension PlatformIO fut une nouveauté, et j'ai donc du prendre en main son utilisation. 

## Création du projet "Blink"

Le projet "Blink" est une initiation à Arduino et à la programmation Bare métal. Pour ce projet nous avons utilisé une carte Arduino Uno, basée sur un ATMega328 cadencé à 16 MHz. <br/> 

Pour la création du projet, nous utilisation l'extension PlatforIO, dans la section "Board", nous sélectionons "Arduino Uno". Le projet se créer et dans le dossier "src" du projet, un main.cpp est présent. Celui-ci est déja composé des fonctionn "setup()" et "loop()" qui sont les fonctions principales de notre programme. La fonction "setup()" permet de paramétrer notre fichier et la fonction "loop()" est exécuter en boucle lors l'exécution du projet. Nous avonc donc modifié c'est deux fonctions durant toute l'évolution de notre projet.

### Etape 1

Dans un premier temps nous avons utilisé la librairie Arduino. Pour cela nous nous sommes rendus sur le site d'Arduino est nous avons utilisé deux fonctions présente dans la libraire :  <br/>
 pinMode(ledPin, OUTPUT), qui permet de définir une pin de l'arduino est mode sortie; <br/>
 digitalWrite(ledPin, HIGH), qui permet d'alllumer la led quand le paramètres est "HIGH" est l'éteindre qund le paramètre est "LOW"; <br/>

La fonction delay(1000), permet de faire attendre pendant 1 seconde. <br/>
Nous avons aussi utilisé "Serial.println("Blink");" qui permet d'afficher dans le moniteur série la valeur "blink" à chaque exécution de la fonction loop(). Cela permet de déboguer notre code. <br/> 

### Etape 2

Maintenant nous n'allons plus utiliser les fonctions de la libraire arduino mais utiliser les ports et registres de notre carte Arduino UNO. <br/>

Pour rappel : </br>
DDR (Data Direction Register) est le registre qui détermine le sens de circulation des informations: </br>
</br>
 - 0 = ENTREE </br>
 - 1 = SORTIE </br>
</br> 
PORT définit l'état de sortie: </br>
</br>
 - 0 = BAS </br>
 - 1 = HAUT </br>
</br> 
PIN permet de récupérer l'état d'entrée: </br>
</br>
 - 0 = BAS </br>
 - 1 = HAUT </br>
</br>
D'après le datasheet de l'arduino UNO, la pin 13 (notre led), correspond à PB5. </br>
Donc nous allons mettre au bit 1 le 5ème bit du port B : LED_MASK (1 << 5). </br>

Pour allumer la LED, fonction led_on(), nous devons appliquer ce décalage : PORTB |=  LED_MASK  ( opérateur OU qui permet d'appliquer ce décalage) </br>
Pour éteindre la LED, fonction led_off() nous allons appliquer l'inverse du décalage : PORTB &= ~ LED_MASK; ( opérateur NON, qui inverse les bits) </br>

Ces deux fonctions remplacent maintenant les fonctions de la librairire Arduino.

### Etape 3

Nous voulons faire clignoter la LED. Pour cela plus besoin de l'éteindre mais seulement utiliser une fonction led_toggle(). </br>
Cette fonction permet d'allumer la LED quans celle-ci est appellée. Donc dans notre fonction loop(), nous utilisons cette fonction. Nous rajoutons aussi un delay(1000) qui permet de faire clignoter la LED.

### Etape 4

Pour cette étape nous allons remplacer les delay par un PRESCALER ( un timer). </br>

On commence par le paramétrage de notre PRESCALER, CS_PRESCALER_1024 ((1 << CS10) | (1 << CS12)), qui correspondant à deux bits du registre TCCR1B. </br>

La fonction " ISR(TIMER1_COMPA_vect) {
  led_toggle();
}" remplace la fonction "loop()" de l'arduino. </br>

# Linux Embarqué

Pour cette partie nous allons créer un système embarqué linux. Pour cela nous allons ré-utiliser la machine virtuelle linux ( avec Oracle VirtualBox). Le but du projet est de flasher ce système embarqué que l'on a créé sur un Raspberry PI 3B.

## Raspberry PI3B

Le Raspberry Pi est un micro-ordinateur monocarte à processeur ARM de la taille d'une carte de crédit. Le modèle Raspberry que nous avons utilisé est le modèle 3B. Celui-ci intègre un module WI-FI et un processeur ARM Cortex-A53 Quad-Core. Le Raspberry bot avec une carte SD, c'est sur cette carte que nous allons flasher notre système embarqué. 


## Buildroot

Buildroot est un projet qui permet de construire facilement un système embarqué et d’en ajuster finement le contenu.Buildroot est simple d'utilisation pour construire un système personnalisé pour Raspberry Pi. Yocto est un autre projet qui permet de réaliser la même chose mais son utlisation est plus complexe.

### Réalisation 

Pour commencer il faut télécharger le projet sur notre machine : https://buildroot.org/download.html </br>

Une fois installé, nous ouvrons un termainal dans le dossier du projet. Nous choissions une configuration par défault qui permet de d'avoir une base sur la configuration. </br> 
"make raspberrypi3_64_defconfig" </br>

Une fois la configuration choissie, nous ouvrons l'interface de configuration : "make menuconfig" </br>
Ce menu de configuration permet de pouvoir accéder à plusieurs paramètres possibles à modifié. </br>
Nous allons rajouter des drivers et des packadges ( nano, module wifi, ...). </br>
Pour rajouter un packadge externe, il faut rajouter sa source dans le dossier "external/package/". </br>
Nous avons aussi modifié le nom, le mot de passe. </br>

Une fois toute ces étapes réalisées, nous pouvons built notre système commande "make". Cette étape peut être longue car nous téléchargons tout les packadges et mises à jours système.

