#include <Arduino.h>
#define LED 13
#define LED_MASK (1 << 5)

void led_setup() {
 
  DDRB |= LED_MASK;
}

void led_toggle(){
  
  PINB = LED_MASK;
}

void setup() {
  Serial.begin(9600);
  led_setup();
  Serial.println("Blink");
}

void loop() {

  led_toggle();
  delay(1000);                          
}

